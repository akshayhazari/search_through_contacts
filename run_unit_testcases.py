#!/usr/bin/python2.7 
import unittest,sys
from cStringIO import StringIO
from contextlib import contextmanager
from SRC_Contact import *
from Search_Through_Contacts import *

@contextmanager
def nostdout():
    save_stdout = sys.stdout
    sys.stdout = StringIO()
    yield
    sys.stdout = save_stdout

class Test_Search(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Test_Search, self).__init__(*args, **kwargs)
        with nostdout():
            self.c = SearchThroughContacts()
            self.contact = Contact()
            self.addContact  = self.c.run_inputs()
            self.addContact.next()
        inp = ['Test 123','Name Lastname','Albert Einstein','Sherlock Holmes','Bhavik Gohil','Jai Ghosh','Kurt Cobain','KuRt CoBaIn','Ace','Ace Ventura','Pace','Chris','Chris Harris','Chris Cairns','Harry Potter','chris cairns']

        map(lambda x: self.send('1',x),inp)
        
    def send(self,*args):
        with nostdout():
            map(lambda x:self.addContact.send(x),args)
    
        
    def test_00(self):
        with self.assertRaises(ValueError):
            self.contact.createContact('firstname lastname extrainfo')

    def test_01(self):
        with self.assertRaises(ValueError):
            self.contact.createContact('123456789012345678901234567890123456789012345678901')
    
    def test_multiple(self):
        searchVal = 'Ku Al Na'
        self.assertEquals(set(['Albert Einstein','KuRt CoBaIn','Kurt Cobain','Name Lastname']),set(map(str,self.c.search(searchVal,0,0,1,0,0))))
    
    def test_advsrch_multiple(self):
        with nostdout():
            searchVal = 'Ku Al Na -f -l -c'
            self.assertEquals(set(['Albert Einstein','KuRt CoBaIn','Kurt Cobain','Name Lastname']),set(map(str,self.c.advsrch(searchVal))))
            searchVal = 'Ku La Na -l -c'
            self.assertEquals(set(['Name Lastname']),set(map(str,self.c.advsrch(searchVal))))
            searchVal = 'Al Co Na -f -c'
            self.assertEquals(set(['Name Lastname','Albert Einstein']),set(map(str,self.c.advsrch(searchVal))))
            
    def test_0(self):
        """Case Sensitive Search for full name with exact match """
        searchVal = 'Chris Cairns'
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.search(searchVal,0,0,0,0,0))))
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.search(searchVal,1,1,0,0,0))))
        searchVal = 'Chris'
        self.assertEquals(set(['Chris']),set(map(str,self.c.search(searchVal,0,0,0,1,0))))
        self.assertEquals(set(['Chris']),set(map(str,self.c.search(searchVal,1,1,0,1,0))))


    def test_1(self):
        '''Case Insensitive Search for full name with exact match'''
        searchVal = 'Chris cairns'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.search(searchVal,0,0,0,0,1))))
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.search(searchVal,1,1,0,0,1))))
        searchVal = 'chris'
        self.assertEquals(set(['Chris']),set(map(str,self.c.search(searchVal,0,0,0,1,1))))
        self.assertEquals(set(['Chris']),set(map(str,self.c.search(searchVal,1,1,0,1,1))))


    def test_4(self):
        '''Case Sensitive Search for full name with partial match'''
        searchVal = 'Ch Ca'
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.search(searchVal,0,0,1,0,0))))
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.search(searchVal,1,1,1,0,0))))
        searchVal = 'ris C'
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.search(searchVal,0,0,1,0,0))))
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.search(searchVal,1,1,1,0,0))))
        searchVal = 'Ace'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,1,1,0,0))))
        searchVal = 'Chris'
        self.assertTrue(set(['Chris','Chris Harris','Chris Cairns']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,1,1,0,0))))
        searchVal = 'Ace'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,0,0,1,0,0))))
        searchVal = 'Chris'
        self.assertTrue(set(['Chris','Chris Harris','Chris Cairns']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,0,0,1,0,0))))

    def test_5(self):
        '''Case Insensitive Search for full name with partial match'''
        searchVal = 'ch irns'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.search(searchVal,0,0,1,0,1))))
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.search(searchVal,1,1,1,0,1))))
        searchVal = 'Ac'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,0,0,1,0,1))))
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,1,1,0,1))))
        searchVal = 'ch ca'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.search(searchVal,1,1,1,0,1))))
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.search(searchVal,0,0,1,0,1))))
        searchVal = 'ace'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,1,1,0,1))))
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,0,0,1,0,1))))
        searchVal = 'chris'
        self.assertTrue(set(['Chris','Chris Harris','Chris Cairns','chris cairns']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,1,1,0,1))))
        self.assertTrue(set(['Chris','Chris Harris','Chris Cairns','chris cairns']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,0,0,1,0,1))))
        
  
#6  
    def test_6(self):
        '''Case Sensitive Search for full name where contact startswith searchVal'''
        searchVal = 'Te'
        self.assertEquals('Test 123',' '.join(map(str,self.c.search(searchVal,0,0,1,1,0))))
        self.assertEquals('Test 123',' '.join(map(str,self.c.search(searchVal,1,1,1,1,0))))
        searchVal = 'Sherlock Holmes'
        self.assertTrue(set(['Sherlock Holmes']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,1,1,1,0))))
        self.assertTrue(set(['Sherlock Holmes']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,0,0,1,1,0))))


    def test_7(self):
        '''Case Insensitive Search for full name where contact startswith searchVal'''
        searchVal = 'te'
        self.assertEquals('Test 123',' '.join(map(str,self.c.search(searchVal,0,0,1,1,1))))
        self.assertEquals('Test 123',' '.join(map(str,self.c.search(searchVal,1,1,1,1,1))))
        searchVal = 'sherlock holmes'
        self.assertTrue(set(['Sherlock Holmes']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,1,1,1,1))))
        self.assertTrue(set(['Sherlock Holmes']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,0,0,1,1,1))))


    def test_8(self):
        """Case Sensitive Search for last name with exact match """
        searchVal = 'Harris'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.search(searchVal,0,1,0,0,0))))
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.search(searchVal,0,1,0,1,0))))

    def test_9(self):
        """Case Insensitive Search for last name with exact match """
        searchVal = 'cairns'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.search(searchVal,0,1,0,0,1))))
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.search(searchVal,0,1,0,1,1))))

    def test_12(self):
        """Case Sensitive Partial Search for last name """
        searchVal = 'Har'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.search(searchVal,0,1,1,0,0))))

    def test_13(self):
        """Case Insensitive Partial Search for last name """
        searchVal = 'har'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.search(searchVal,0,1,1,0,1))))
        searchVal = 'rris'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.search(searchVal,0,1,1,0,1))))
    
    def test_14(self):
        """Case Sensitive Partial Search for last name where contact startswith searchVal  """
        searchVal = 'Har'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.search(searchVal,0,1,1,1,0))))
        searchVal = 'Cai'
        self.assertEquals(set(['Chris Cairns']),set(map(str,self.c.search(searchVal,0,1,1,1,0))))
    
    def test_15(self):
        """Case Insensitive Partial Search for last name where contact startswith searchVal  """
        searchVal = 'har'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.search(searchVal,0,1,1,1,1))))
        searchVal = 'cai'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.search(searchVal,0,1,1,1,1))))

    def test_16(self):
        '''Case Sensitive first name search with exact match'''
        searchVal = 'Ace'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,0,0,0))))
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,0,1,0))))

    def test_17(self):
        '''Case Insensitive first name search with exact match'''
        searchVal = 'ace'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,0,0,1))))
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,0,1,1))))

    def test_20(self):
        '''Case Sensitive first name search where contact contains searchVal'''
        searchVal = 'Ac'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,1,0,0))))
        searchVal = 'ce'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,1,0,0))))
    
    def test_21(self):
        '''Case Insensitive first name search where contact contains searchVal'''
        searchVal = 'ace'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,1,0,1))))
        searchVal = 'urt'
        self.assertTrue(set(['Kurt Cobain','KuRt CoBaIn']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,1,0,1))))
    
    def test_22(self):
        '''Case Sensitive first name search where contact startswith searchVal'''
        searchVal = 'Ac'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,1,1,0))))
        searchVal = 'Ku'
        self.assertTrue(set(['Kurt Cobain','KuRt CoBaIn']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,1,1,0))))

    def test_23(self):
        '''Case Insensitive first name search where contact startswith searchVal'''
        searchVal = 'ac'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,1,1,1))))
        searchVal = 'Kur'
        self.assertTrue(set(['Kurt Cobain','KuRt CoBaIn']) == set(map(lambda x: str(x).strip(),self.c.search(searchVal,1,0,1,1,1))))

    def test_advsrch0(self):
        with self.assertRaises(ValueError):
            self.c.advsrch('firstname -e lastname')
        with self.assertRaises(ValueError):
            self.c.advsrch('firstname -i lastname')
        with self.assertRaises(ValueError):
            self.c.advsrch('firstname -c lastname')
        with self.assertRaises(ValueError):
            self.c.advsrch('firstname -f lastname')
        with self.assertRaises(ValueError):
            self.c.advsrch('firstname -l lastname')

    def test_advsrch1(self):
        """Case Sensitive Search for full name with exact match """
        searchVal = 'Chris Cairns -f -l -e'
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'Chris Cairns -e'
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.advsrch(searchVal))))

    def test_advsrch2(self):
        '''Case Insensitive Search for full name with exact match'''
        searchVal = 'Chris cairns -f -l -e -i'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.advsrch(searchVal))))

        searchVal = 'Chris cairns -e -i'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.advsrch(searchVal))))
        
        searchVal = 'chris -f -l -e -i'
        self.assertEquals('Chris',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'chris -e -i'
        self.assertEquals(set(['Chris']),set(map(str,self.c.advsrch(searchVal))))

    def test_advsrch4(self):
        '''Case Sensitive Search for full name with partial match'''
        searchVal = 'Ch Ca -f -l -c'
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'Ch Ca -c'
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'ris C -f -l -c'
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'ris C -c'
        self.assertEquals('Chris Cairns',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'Ace -c'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Ace -c -f -l'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Chris -f -l -c'
        self.assertTrue(set(['Chris','Chris Harris','Chris Cairns']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Chris -c'
        self.assertTrue(set(['Chris','Chris Harris','Chris Cairns']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Ace -f -l -c'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Ace -c '
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))

    def test_advsrch5(self):
        '''Case Insensitive Search for full name with partial match'''
        searchVal = 'ch irns -f -l -c -i'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.advsrch(searchVal))))
        searchVal = 'ch irns -c -i'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.advsrch(searchVal))))
        searchVal = 'Ac -f -l -c -i'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Ac -c -i'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'ch ca -f -l -c -i'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.advsrch(searchVal))))
        searchVal = 'ch ca -c -i'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.advsrch(searchVal))))
        searchVal = 'ace -f -l -c -i'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'ace -c -i'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))

        searchVal = 'chris -f -l -c -i'
        self.assertTrue(set(['Chris','Chris Harris','Chris Cairns','chris cairns']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'chris -c -i'
        self.assertTrue(set(['Chris','Chris Harris','Chris Cairns','chris cairns']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        
    def test_advsrch6(self):
        '''Case Sensitive Search for full name where contact startswith searchVal'''
        searchVal = 'Te -f -l'
        self.assertEquals('Test 123',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'Te'
        self.assertEquals('Test 123',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'Sherlock Holmes -f -l'
        self.assertTrue(set(['Sherlock Holmes']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Sherlock Holmes'
        self.assertTrue(set(['Sherlock Holmes']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))


    def test_advsrch7(self):
        '''Case Insensitive Search for full name where contact startswith searchVal'''
        searchVal = 'te -i -f -l'
        self.assertEquals('Test 123',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'te -i'
        self.assertEquals('Test 123',' '.join(map(str,self.c.advsrch(searchVal))))
        searchVal = 'sherlock holmes -i'
        self.assertTrue(set(['Sherlock Holmes']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'sherlock holmes -i -f -l'
        self.assertTrue(set(['Sherlock Holmes']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))

    def test_advsrch8(self):
        """Case Sensitive Search for last name with exact match """
        searchVal = 'Harris -l -e'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.advsrch(searchVal))))
        searchVal = 'Harris -l -e -c'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.advsrch(searchVal))))

    def test_advsrch9(self):
        """Case Insensitive Search for last name with exact match """
        searchVal = 'cairns -l -e -i'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.advsrch(searchVal))))
        searchVal = 'cairns -l -e -c -i'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.advsrch(searchVal))))


    def test_advsrch12(self):
        """Case Sensitive Partial Search for last name """
        searchVal = 'Har -l -c'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.advsrch(searchVal))))

    def test_advsrch13(self):
        """Case Insensitive Partial Search for last name """
        searchVal = 'har -l -i -c'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.advsrch(searchVal))))
        searchVal = 'rris -l -i -c'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.advsrch(searchVal))))
    
    def test_advsrch14(self):
        """Case Sensitive Partial Search for last name where contact startswith searchVal  """
        searchVal = 'Har -l '
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.advsrch(searchVal))))
        searchVal = 'Cai -l'
        self.assertEquals(set(['Chris Cairns']),set(map(str,self.c.advsrch(searchVal))))
    
    def test_advsrch15(self):
        """Case Insensitive Partial Search for last name where contact startswith searchVal  """
        searchVal = 'har -l -i'
        self.assertEquals(set(['Chris Harris']),set(map(str,self.c.advsrch(searchVal))))
        searchVal = 'cai -l -i'
        self.assertEquals(set(['Chris Cairns','chris cairns']),set(map(str,self.c.advsrch(searchVal))))


    def test_advsrch16(self):
        '''Case Sensitive first name search with exact match'''
        searchVal = 'Ace -e -f'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Ace -e -f -c'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))

    def test_advsrch17(self):
        '''Case Insensitive first name search with exact match'''
        searchVal = 'ace -e -f -i'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'ace -e -f -i -c'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))


    def test_advsrch20(self):
        '''Case Sensitive first name search where contact contains searchVal'''
        searchVal = 'Ac -f -c'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'ce -f -c'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))

    def test_advsrch21(self):
        '''Case Insensitive first name search where contact contains searchVal'''
        searchVal = 'ace -f -c -i'
        self.assertTrue(set(['Ace','Ace Ventura','Pace']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'urt -f -c -i'
        self.assertTrue(set(['Kurt Cobain','KuRt CoBaIn']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
    
    def test_advsrch22(self):
        '''Case Sensitive first name search where contact startswith searchVal'''
        searchVal = 'Ac -f'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Ku -f'
        self.assertTrue(set(['Kurt Cobain','KuRt CoBaIn']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))

    def test_advsrch23(self):
        '''Case Insensitive first name search where contact startswith searchVal'''
        searchVal = 'ac -f -i'
        self.assertTrue(set(['Ace','Ace Ventura']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))
        searchVal = 'Kur -f -i'
        self.assertTrue(set(['Kurt Cobain','KuRt CoBaIn']) == set(map(lambda x: str(x).strip(),self.c.advsrch(searchVal))))

        
if __name__ == '__main__':
    unittest.main()

    
    
