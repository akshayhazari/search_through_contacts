----- Search Through Contacts -----

1. run_unit_testcases.py : 
		      This file contains test cases which test the execution of the code.


2. Search_Through_Contacts.py :
			   You can create contacts , search through the contacts , save/load contacts to/from file.
		
   . There are two modes of search:

     . Normal Search : 
 		     This enables default searching . Where search checks contacts starting with a particular pattern.
		     When two patterns are entered in search it checks the first pattern in first name and second in last name and returns when both match.  
		     When more than two patterns are entered it checks each pattern separately whether any pattern matches the contact.
		     All results return exact matches first followed by other matches.

     . Advanced Search : 
       		       It gives multiple options to search. Which can be combined to give appropriate results.
		       -f : Check pattern in first name.
		       -l : Check pattern in last name.
		       -e : Return only exact matches.
		       -c : Check if contact contains pattern. ( Default :Patterns are searched from the start of the contact. Whether contact startswith)
		       -i : Case Insensitive Search.


