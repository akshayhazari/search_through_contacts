#!/usr/bin/env python2.7
import sys
from collections import defaultdict
from SRC_Contact import *

class SearchThroughContacts:
    '''Executes user inputs to add a contact,search through the contacts. Save and load contacts.'''
    def __init__(self,save_load = 0):
        self.user_input = None
        self.firstname = defaultdict(list)
        self.lastname = defaultdict(list)
        self.fullname = defaultdict(list)
        self.DCI = self.displayContactInfo()
        self.DCI.next()
        self.AC = self.addContact()
        self.AC.next()
        self.save_load = save_load
        self.adv = False
        self.sl = False
        self.fp = 'savedContacts.p'
        if save_load ==1:
            global dump , load
            from pickle import dump, load
        
    def saveContacts(self,fn):
        '''Saves the contacts to a file. To be retrieved later.'''
        try:
            allData = defaultdict(list)
            allData['firstname'] = self.firstname
            allData['lastname'] = self.lastname
            allData['fullname'] = self.fullname
            dump(allData, open( fn, "wb" ))
        except:
            print 'Error saving to file'
            pass
        
    def loadContacts(self,fn):
        '''Loads the previously saved contacts from a file.'''
        try :
            allData=load(open(fn,'rb'))
            self.firstname = allData['firstname']
            self.lastname = allData['lastname']
            self.fullname = allData['fullname']
        except:
            print '\nError Loading File.\n'
            pass
        
    def addFirstname(self,firstname,contactInfo):
        '''Adds the first name to the dictionary'''
        self.firstname[firstname] += [str(contactInfo)]

    def addLastName(self,lastname,contactInfo):
        '''Adds the last name to the dictionary'''
        self.lastname[lastname] += [str(contactInfo)]
        
    def addFullName(self,fullname,contactInfo):
        '''Adds the full name to the dictionary'''
        self.fullname[fullname] += [str(contactInfo)]

    def addToDict(self,contact):
        '''Adds the contact info to the dictionary for look up.'''
        if contact.firstname:
            self.addFirstname(contact.firstname,contact)
        if contact.lastname:
            self.addLastName(contact.lastname,contact)
        if contact.fullname:
            self.addFullName(contact.fullname,contact)

    def displayContactInfo(self):
        '''Displays each contact object.'''
        while True:
            contact = (yield)
            print contact


    def search(self,searchVal,searchByFirstName=1,searchByLastName=1,searchPartial=1,searchStartsWith=1,caseInsensitive=0):
        '''Main search function to search through the contacts based on fullname, firstname, lastname, checks whether contact contains a search pattern, check if contact starts with pattern, Case insensitive search.'''
        foundContacts,keys = [],[]
        searchBy,fln = self.fullname,1
        if (searchByFirstName or searchByLastName) and not(searchByFirstName and searchByLastName):
            fln=0
            if searchByFirstName :
                searchBy = self.firstname
            elif searchByLastName :
                searchBy = self.lastname

        '''Function to check Exact Match'''

        checkExact = lambda pattern,namepart=None: pattern==namepart

        '''Function to check Partial Match'''

        checkPartial = lambda pattern,namepart=None: (pattern in namepart if namepart else False) 

        '''Function to check StartsWith'''

        checkStartsWith = lambda pattern,namepart=None: (namepart.startswith(pattern) if namepart else False)

        '''Function to check case Insensitive'''

        checkInsensitive = lambda ins,function,pattern,namepart=None:(function(pattern.lower(),namepart.lower()) if ins else function(pattern,namepart)) if namepart else False

        '''Combining all functions into one'''

        checkAll = lambda ins,partial,startswith,pattern,namepart=None: (checkInsensitive(ins,checkStartsWith,pattern,namepart) if startswith else checkInsensitive(ins,checkPartial,pattern,namepart)) if partial else checkInsensitive(ins,checkExact,pattern,namepart)
        
        orand = lambda x,y,z : x or y if z else x and y 

        checkAllFnLn = lambda ins,partial,startswith,pattern,or_and,firstname,lastname=None: orand(checkAll(ins,partial,startswith,pattern,firstname),checkAll(ins,partial,startswith,pattern,lastname),or_and)

        if fln:
            if len(searchVal.split())==2:
                srchVal1,srchVal2 = searchVal.split()
                keys= [i for i in searchBy.keys() if ((checkAll(caseInsensitive,searchPartial,searchStartsWith,srchVal1,i.split()[0]) and checkAll(caseInsensitive,searchPartial,searchStartsWith,srchVal2,i.split()[1])) if len(i.split())==2 else False)] 
            else:
                searchVals=searchVal.split()
                allkeys = searchBy.keys()
                fullnameIndices=set()
                if len(searchVals)==1 and not searchPartial:
                    keys = [i for i in allkeys if checkAll(caseInsensitive,searchPartial,searchStartsWith,searchVal,i)]
                else:
                    for i in searchVals:
                        fullnameIndices.update(map(lambda x: x[0],filter(lambda akey:checkAllFnLn(caseInsensitive,searchPartial,searchStartsWith,i,1,*akey[1].split()),list(enumerate(allkeys)))))
                    keys=map(lambda x:allkeys[x],fullnameIndices)
        else:
            if len(searchVal.split())==1:
                keys = [i for i in searchBy.keys() if checkAll(caseInsensitive,searchPartial,searchStartsWith,searchVal,i)] 
            else:
                for k in searchVal.split():
                    keys+= [i for i in searchBy.keys() if checkAll(caseInsensitive,searchPartial,searchStartsWith,searchVal,i)]
        reordering= reduce(lambda z,y : (z[0]+[y],z[1]) if y==searchVal else (z[0],z[1]+[y]) , keys,([],[]))
        reordering=reordering[0]+reordering[1]
        keys = reordering
        for key in keys:
            foundContacts+=[searchBy[key]]
        return reduce(lambda x, y : x+ y ,foundContacts,[])

    def addContact(self):
        ''' Creats a contact and adds to the dictionary for lookup.'''
        while True:
            contactInfo = (yield)
            self.user_input = contactInfo
            c=Contact()
            try :
                c.createContact(contactInfo)
                self.addToDict(c)
            except Exception as err:
                print 'Exception : ' + str(err)
                pass
            
    def help(self,adv=0):
        '''Provides instructions for user inputs.'''
        if adv==0:
            sl = '''6) Save to contacts to file to be retrieved later\n\n7) Load contacts from file saved earlier\n\n''' if self.sl else ''
            return """\n------------Help : Search Through Contacts------------\n\nValid inputs are 0 1 2 3 4 5 \n\n0) Help\n\n1) Add contact: Adds a Contact.\n\n\t eg: Input : Milan Kundera\n\n2) Search: Search through a contact\n\n\t eg: Input : Mi \t Output : Milan Kundera\n\n3) Exit: Quit \n\n4) Enable Advanced Search Mode\n\n5) Enable/Disable Load/Save Contacts\n\n{}Note : if number of search Strings > 2 then each String is considered as a new search pattern.\n\n""".format(sl)
        else:
            sl = '''6) Save to contacts to file to be retrieved later\n\n7) Load contacts from file saved earlier\n\n''' if self.sl else ''
            return """\n\t------------Help : Search Through Contacts------------\n\nValid inputs are 0 1 2 3 4 5 6 7\n\n0) Help\n\n1) Add contact: Adds a Contact.\n\n\t eg: Input : Milan Kundera\n\n2) Search: Search through a contact\n\n Syntax : SearchValue <Options>\n\n Options : \n -l : Search in last name \n -f : Search in First Name  \n -e : Search Exact Match \n -c : Check if contact contains pattern (Default is search contact Startswith pattern) \n\n -i : Case Insensitive \n eg: Input : Mi \t Output : Milan Kundera\n \n eg: Input : Mi -f\t Output : Milan Kundera\n \n eg: Input : Ku -l \t Output : Milan Kundera\n\n eg: Input : la -f -c\t Output : Milan Kundera\n \n eg: Input : Milan -f -e\t Output : Milan Kundera\n\n3) Exit: Quit \n\n4) Enable/Disable Advanced Search Mode\n\n5) Enable/Disable Load/Save Contacts\n\n{} Note : -c check if contact contains pattern and default is startswith. Using both -f -l is default search . If using default search and number of search Strings > 2 then each String is considered as a new search pattern. Similarly for -l or -f when Search Strings are > 1 then each String is considered as a new search pattern\n\n""".format(sl)
            
    def advsrch(self,srch):
        '''Parses advanced search input. -f for search in first name, -l for search in last name , -e for exact search , -i for case insensitive, -c for check if contact contains pattern.'''

        srch=srch.split()
        srchPatterns = [i for i,j in enumerate(srch) if j!='-l' and j!='-f' and j!='-e' and j!='-c' and j!='-i' ]
        srchArgs = [i for i,j in enumerate(srch) if j=='-l' or j=='-f' or j=='-e' or j=='-c' or j=='-i']
        
        invalid =  [(i,j) for i in srchArgs for j in srchPatterns if i< j]
        f = 1 if ('-f' in srch ) else 0
        l = 1 if ('-l' in srch ) else 0
        e = 0 if ('-e' in srch ) else 1
        s = 0 if ('-c' in srch ) else 1
        ins = 1 if ('-i' in srch ) else 0
        fl = 1 if (f and l) or (f==0 and l==0) else 0

        if invalid:
            raise ValueError('{} does not fit the syntax - SearchPatterns [Options] (with valid Options -f -l -e -c)'.format(' '.join(srch)))
        else:
            if fl :
                if len(srchPatterns)>2 :
                    srchP = [srch[i] for i in srchPatterns]
                    cnt = 1
                    advres = []
                    for i in srchP:
                        print 'Providing result for pattern {} {}'.format(cnt,i)
                        advres += self.search(i,f,l,e,s,ins)
                        cnt+=1
                else:
                    srchP = [srch[i] for i in srchPatterns]
                    advres = self.search(' '.join(srchP),f,l,e,s,ins)
                return advres
            else:
                srchP = [srch[i] for i in srchPatterns]
                cnt = 1
                advres = []
                for i in srchP:
                    if len(srchP)>1:
                        print 'Providing result for pattern {}) {}'.format(cnt,i)
                    advres += self.search(i,f,l,e,s,ins) 
                    cnt+=1
                return advres

         
    def run_inputs(self):
        while True:
            if self.sl:
                print "\n\n0) Help 1) Add contact 2) Search 3) Exit 4) Enable/Disable Advanced Search 5) Enable/Disable Save/Load Contacts 6) Save Contacts 7) Load Contacts :"
            else:
                print "\n\n0) Help 1) Add contact 2) Search 3) Exit 4) Enable/Disable Advanced Search 5) Enable/Disable Save/Load Contacts :"
            inp = (yield)
            inp = inp.strip()
            if inp.isdigit():
                inp = int(inp)
                if inp == 1:
                    inp =(yield)
                    self.AC.send(inp)
                elif inp ==2:
                    inp = (yield)
                    srch=inp
                    if self.adv:
                        if srch:
                            try :
                                for i in self.advsrch(srch):
                                    self.DCI.send(i)
                            except Exception as err:
                                print 'Exception : ' + str(err)
                                pass
                        else:
                            print '\nPlease provide an input\n'
                    else:
                        for i in self.search(srch):
                            self.DCI.send(i)
                elif inp ==3:
                    print '\n\nHappy Searching\n\n'
                    sys.exit()
                elif inp ==4:
                    self.adv = not self.adv
                    pnt = "Advanced Search Enabled. Check Help for more details." if self.adv else "Advanced Search Disabled."
                    print pnt
                elif inp==5:
                    if not self.sl:
                        global dump , load
                        from pickle import dump, load
                    self.sl = not self.sl
                    pnt = "Save/Load Enabled. Check Help for more details." if self.sl else "Save/Load Disabled."
                    print pnt    
                else :
                    if self.sl:
                        if inp == 6:
                            sys.stdout.write("Enter File Path:")
                            inp = (yield)
                            fp=inp
                            self.saveContacts(fp)
                        elif inp == 7:
                            sys.stdout.write("Enter File Path:")
                            inp = (yield)
                            fp=inp
                            self.loadContacts(fp)
                        else:
                            if inp!=0:
                                print '\nUnknown Input!!!\n'+self.help(self.adv)+'\nUnknown Input!!!\n'
                            print self.help(self.adv)
                    else:
                        if inp!=0:
                            print '\nUnknown Input!!!\n'+self.help(self.adv)+'\nUnknown Input!!!\n'
                        print self.help(self.adv)
            else:
                print '\nUnknown Input!!!\n'+self.help(self.adv)+'\nUnknown Input!!!\n'

    def execute(self):
        addContact =self.run_inputs()
        addContact.next()
        while True:
            addContact.send(raw_input())

        
if __name__ == '__main__':
    c= SearchThroughContacts()
    c.execute()


