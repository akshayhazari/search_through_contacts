#!/usr/bin/env python2.7
import sys
from collections import defaultdict

class Contact:
    def __init__(self):
        self.fullname = None
        self.lastname = None
        self.firstname = None
        self.contactInfo = None
        self.maxContactInfoValues = 2

    def _isValid_(self,contactInfo):
        '''Checks if the contact string input is valid''' 
        return  (1 if len(contactInfo)<51 else 2) if reduce(lambda x,y: x+1 if y.isspace() else x+0 ,contactInfo,0) < self.maxContactInfoValues else 0

    def _extractFullName_(self,contactInfo):
        '''Extracts the full name from contact info'''
        return contactInfo

    def createContact(self,contactInfo):
        '''Creates the contact object by setting the contact info'''
        contactInfo = ' '.join(contactInfo.split())
        isvalid = self._isValid_(contactInfo)
        if not isvalid:
            raise ValueError("As of now only the first and last name is required.")
        elif isvalid==2:
            raise ValueError("Contact can be upto 50 characters.")
        self.fullname = self._extractFullName_(contactInfo)
        self.firstname,self.lastname= self.fullname.split() if len(self.fullname.split()) >1 else (self.fullname,None)
        
    def __str__(self):
        '''Converts the contact object to string. Prints the important parts of contact object.'''
        xstr = lambda x: x or ''
        return ("{} {}".format(xstr(self.firstname),xstr(self.lastname))).strip()
